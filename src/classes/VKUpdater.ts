import {VKService} from "../helpers/vk";
import Country from "../models/country.model";
import City from "../models/cities.model";

export default class VKUpdater {
    constructor() {
        this.update().then(() => {
            console.log('Init VK checker completed')
        });
        setInterval(this.update, 1000 * 60 * 10) //10 минут
    }

    async update() {
        const {items: countries} = await VKService.api.database.getCountries({
            count: 1000,
            need_all: 1
        });
        const DBCountries = await Country.findAll();

        await Promise.all(countries.map(async (country) => {
            let DBCountry: Country = DBCountries.find(x => x.countryId === country.id);
            try {
                if (!DBCountry) DBCountry = await Country.create({countryId: country.id, name: country.title});
            } catch (e) {
                console.error(e);
            }

            const {items: cities} = await VKService.api.database.getCities({
                country_id: DBCountry.countryId,
                need_all: 0,
                count: 1000
            });

            const DBCities = await City.findAll({
                where: {
                    countryId: DBCountry.id
                }
            });

            await Promise.all(cities.map(async (city) => {
                if (DBCities.find(x => x.cityId === city.id)) return;
                try {
                    await City.create({
                        countryId: DBCountry.id,
                        cityId: city.id,
                        name: city.title
                    })
                } catch (e) {
                    console.error(e);
                }
            }))
        }));
    }
}
