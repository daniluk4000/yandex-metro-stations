import Station from "../models/station.model";
import {VKService} from "../helpers/vk";
import City from "../models/cities.model";
import Place from "../models/place.model";
import {addLog, replaceAll} from "../helpers";
import requestPromise = require("request-promise");

export default class StationsUpdated {
    constructor() {
        this.update().then(() => {
            console.log('Init Stations checker completed')
        });
        setInterval(this.update, 1000 * 60) //Минута
    }

    async update() {
        const stations = await Station.findAll({
            include: [
                {
                    model: City,
                    required: true
                }
            ]
        });

        await Promise.all(stations.map(async (station) => {
            try {
                if (!station.exists) {
                    const {items} = await VKService.api.database.getMetroStations({
                        city_id: station.city.cityId,
                        count: 500,
                        extended: 1
                    });

                    await addLog({
                        body: {
                            city_id: station.city.cityId,
                            count: 500,
                            extended: 1
                        },
                        response: items,
                        type: "findMetro",
                        source: "vk"
                    })

                    const VKStation = items.find(x => x.name.toLowerCase().includes(station.name.toLowerCase()));

                    if (VKStation) {
                        station.name = VKStation.name;
                        station.lineColor = `#${VKStation.color}`;
                        station.exists = true;
                        await station.save();
                    }
                }

                if (!station.exists) return;

                if (!station.lat || !station.lon) {
                    const {response} = await requestPromise({
                        qs: {
                            apikey: '9fa90fbc-ce5f-4dc9-ae6d-433e0ec7338b',
                            geocode: replaceAll(station.name, ' ', '+'),
                            kind: 'metro',
                            format: 'json',
                            results: '1',
                            lang: 'ru_RU'
                        },
                        uri: `https://geocode-maps.yandex.ru/1.x/`,
                        json: true
                    });

                    await addLog({
                        body: {
                            apikey: '9fa90fbc-ce5f-4dc9-ae6d-433e0ec7338b',
                            geocode: replaceAll(station.name, ' ', '+'),
                            kind: 'metro',
                            format: 'json',
                            results: '1',
                            lang: 'ru_RU'
                        },
                        response,
                        type: "findCoordinates",
                        source: "yandex"
                    })

                    const featureMember = response?.GeoObjectCollection?.featureMember;
                    if (!featureMember) return;

                    const member = featureMember[0];
                    if (!member) return;

                    const {pos} = member.GeoObject?.Point;
                    if (!pos) return;

                    const [lon, lat] = pos.split(' ');
                    station.lon = lon;
                    station.lat = lat;
                    await station.save();
                }

                const places = await Place.findAll({
                    where: {
                        stationId: station.id
                    }
                });

                if (!places.length) {
                    const {features} = await requestPromise({
                        uri: 'https://search-maps.yandex.ru/v1/',
                        qs: {
                            apikey: 'e405f626-afea-444e-9bff-26b8529627dc',
                            text: 'restaurant',
                            lang: 'ru_RU',
                            type: 'biz',
                            ll: `${station.lon},${station.lat}`,
                            spn: `0.010000,0.010000`,
                            results: 500,
                        },
                        json: true
                    });

                    await addLog({
                        body: {
                            apikey: 'e405f626-afea-444e-9bff-26b8529627dc',
                            text: 'restaurant',
                            lang: 'ru_RU',
                            type: 'biz',
                            ll: `${station.lon},${station.lat}`,
                            spn: `0.010000,0.010000`,
                            results: 500,
                        },
                        response: features,
                        type: "findOrganization",
                        source: "yandex"
                    })

                    await Promise.all(features.map(async (feature) => {
                        try {
                            const {name: placeName} = feature.properties.CompanyMetaData;
                            const [lon, lat] = feature.geometry.coordinates;
                            await Place.create({placeName, lon, lat, stationId: station.id});
                        } catch (e) {
                            console.error(e);
                        }
                    }))
                }
            } catch (e) {
                console.error(e);
            }
        }))
    }
}
