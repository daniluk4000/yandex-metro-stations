import {requestType, sourceType} from "../types";
import Log from "../models/logs.model";

export function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

export async function addLog(options: {
    body?: { [key: string]: any } | Array<any>,
    type: requestType,
    response: { [key: string]: any } | Array<any>,
    status?: number,
    source?: sourceType
}): Promise<number> {
    try {
        const {body, type, status, source, response} = options;
        const log = await Log.create({
            request: body, type, status, source, response
        })
        return log.id;
    } catch (e) {
        console.error(e);
        return 0;
    }
}
