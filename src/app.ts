import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from 'cors';

import errorRouter from "./routes/errors";
import {indexRouter} from "./routes";
import sequelize from "./models/connection";
import VKUpdater from "./classes/VKUpdater";
import StationUpdater from "./classes/StationUpdater";

class App {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();

        sequelize.sync({force: false}).then(async () => {
            console.log('Sync has been completed');
            new VKUpdater();
            new StationUpdater()
        });
    }

    private config(): void {
        this.app.use(bodyParser.json({limit: '1mb'}));
        this.app.use(cors());

        this.app.use('/', indexRouter);

        this.app.use(errorRouter);
    }

}

export default new App().app;
