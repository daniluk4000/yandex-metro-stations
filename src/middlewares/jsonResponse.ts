import {DefaultRequest, jsonResponse, respError} from "../types";
import {addLog} from "../helpers";

export default function jsonResponseDefault(func: (req?: DefaultRequest) => jsonResponse) {
    return async function (req: DefaultRequest, res, next) {
        await func(req).then(async (resp) => {
            res.status(resp.code || 200);
            if (resp.status === 0) {
                const {params, body, query} = req;

                let curBody: { [key: string]: object } = {
                    ...params,
                    ...body,
                    ...query
                };

                const respSerialized: respError = JSON.parse(JSON.stringify(resp));
                delete respSerialized.code;

                await addLog({
                    body: curBody,
                    status: resp.code,
                    type: resp.type,
                    response: respSerialized
                })
            }
            if (resp.code) delete resp.code;
            res.json(resp);
        }).catch((e) => {
            req.error = e;
            next();
        })
    }
}
