import {Request} from 'express';

export type DefaultRequest = Request & { error: Error }

interface respSuccess {
    status: 1,
    code?: number,

    [key: string]: any
}

interface respError {
    status: 0,
    code: number,
    error: string,
    type: requestType

    [key: string]: any
}

type responseType = respSuccess | respError;

export type jsonResponse = Promise<responseType>

export type requestType =
    'addStation'
    | 'deleteStation'
    | 'editStation'
    | 'getCountries'
    | 'getCountry'
    | 'getStation'
    | 'getStations'
    | 'findOrganization'
    | 'findCoordinates'
    | 'findMetro'

export type sourceType = 'public' | 'yandex' | 'vk'
