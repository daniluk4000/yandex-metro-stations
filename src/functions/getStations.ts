import {jsonResponse} from "../types";
import Station from "../models/station.model";
import City from "../models/cities.model";
import Country from "../models/country.model";
import {defaultRequest} from "../routes/errors";
import {Op, WhereOptions} from "sequelize";
import {addLog} from "../helpers";

export default async function ({query}: defaultRequest): jsonResponse {
    const where: WhereOptions = {}

    if (query.name) {
        where.name = {
            [Op.iLike]: `%${query.name.trim()}%`
        }
    }

    if (query.city) {
        where.cityId = query.city.trim()
    }

    const stations = await Station.findAll({
        where,
        order: [['id', 'DESC']],
        include: [
            {
                model: City,
                include: [
                    {
                        model: Country
                    }
                ]
            }
        ]
    })

    await addLog({
        body: query,
        type: "getStations",
        response: {
            status: 1,
            stations
        }
    })

    return {
        status: 1,
        stations
    }
}
