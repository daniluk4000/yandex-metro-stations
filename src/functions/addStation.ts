import {DefaultRequest, jsonResponse} from "../types";
import Station from "../models/station.model";
import {Op} from "sequelize";
import {addLog} from "../helpers";

export default async function ({body}: DefaultRequest): jsonResponse {
    let {name, city_id: cityId, line_color: lineColor, lon, lat} = body;
    name = name.trim();

    const station = await Station.findOne({
        where: {
            name: {
                [Op.iLike]: `%${name}%`
            }
        }
    });
    if (station) return {
        status: 0,
        code: 400,
        error: "Такая станция уже существует",
        type: "addStation"
    };

    let result: Station;

    if (!lon || !lat) result = await Station.create({name, cityId, lineColor});
    else result = await Station.create({name, lineColor, cityId, lon, lat});

    await addLog({
        body,
        type: "addStation",
        response: {
            status: 1,
            station: result.id,
        }
    })

    return {
        status: 1,
        station: result.id,
    }
}
