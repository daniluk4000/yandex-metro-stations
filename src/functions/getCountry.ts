import {DefaultRequest, jsonResponse} from "../types";
import Country from "../models/country.model";
import City from "../models/cities.model";
import {addLog} from "../helpers";

export default async function ({params}: DefaultRequest): jsonResponse {
    const country = await Country.findByPk(params.id || 0, {
        include: [
            {
                model: City,
                order: [['cityId', 'DESC']]
            }
        ]
    });

    if (!country) return {
        status: 0,
        code: 404,
        error: "Страна не найдена",
        type: "getCountry"
    };

    await addLog({
        body: {id: params.id},
        type: "getCountry",
        response: {
            status: 1,
            country
        }
    })

    return {
        status: 1,
        country
    }
}
