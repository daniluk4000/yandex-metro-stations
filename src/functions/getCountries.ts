import {jsonResponse} from "../types";
import Country from "../models/country.model";
import {addLog} from "../helpers";

export default async function (): jsonResponse {
    const countries = await Country.findAll({
        order: [['countryId', 'DESC']]
    });

    await addLog({
        type: "getCountries",
        response: {
            status: 1,
            countries
        }
    })
    return {
        status: 1,
        countries
    }
}
