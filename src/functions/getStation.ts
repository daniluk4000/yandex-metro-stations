import {jsonResponse} from "../types";
import Station from "../models/station.model";
import {defaultRequest} from "../routes/errors";
import Place from "../models/place.model";
import City from "../models/cities.model";
import Country from "../models/country.model";
import {addLog} from "../helpers";

export default async function ({params}: defaultRequest): jsonResponse {
    const station = await Station.findByPk(params.id || 0, {
        include: [
            {
                model: Place,
                required: false
            },
            {
                model: City,
                include: [
                    {
                        model: Country
                    }
                ]
            }
        ]
    });

    if (!station) return {
        status: 0,
        code: 404,
        error: "Станции с таким ID не существует",
        type: "getStation",
    };

    await addLog({
        body: {id: params.id,},
        type: "getStation",
        response: {
            status: 1,
            station
        }
    })

    return {
        status: 1,
        station
    }
}
