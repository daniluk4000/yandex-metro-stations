import {defaultRequest} from "../routes/errors";
import {jsonResponse} from "../types";
import Station from "../models/station.model";
import Place from "../models/place.model";
import {Op} from "sequelize";
import {addLog} from "../helpers";

export default async function ({body, params}: defaultRequest): jsonResponse {
    const id = params.id || 0;
    let {name, line_color} = body;
    if (name) {
        name = name.trim();
        const station = await Station.findOne({
            where: {
                name: {
                    [Op.iLike]: `%${name}%`
                },
                id: {
                    [Op.ne]: id
                }
            }
        });
        if (station) return {
            status: 0,
            code: 400,
            error: "Такая станция уже существует",
            type: "editStation"
        };
    }
    const station = await Station.findByPk(id, {
        include: [
            {
                model: Place,
                required: false
            }
        ]
    });

    if (!station) return {
        status: 0,
        code: 404,
        error: "Станции с таким ID не существует",
        type: "editStation"
    };

    if (station.exists) return {
        status: 0,
        code: 401,
        error: "Вы не можете изменять уже существующую станцию",
        type: "editStation"
    };

    if (name)
        station.name = name;

    if (line_color)
        station.lineColor = line_color;

    await station.save();

    await addLog({
        body: {...body, id: params.id},
        type: "editStation",
        response: {
            status: 1,
            station
        }
    })

    return {
        status: 1,
        station
    }
}
