import {jsonResponse} from "../types";
import Station from "../models/station.model";
import {defaultRequest} from "../routes/errors";
import Place from "../models/place.model";
import {addLog} from "../helpers";

export default async function ({params, body}: defaultRequest): jsonResponse {
    const station = await Station.findByPk(params.id || 0, {
        include: [
            {
                model: Place,
                required: false
            }
        ]
    });

    if (!station) return {
        status: 0,
        code: 404,
        error: "Станции с таким ID не существует",
        type: "deleteStation"
    };

    await station.destroy();

    await addLog({
        body: {...body, id: params.id},
        type: "deleteStation",
        response: {
            status: 1
        }
    })

    return {
        status: 1
    }
}
