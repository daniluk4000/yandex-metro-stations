import * as express from "express";

const router = express.Router();

router.use((req: defaultRequest, res) => {
    if (req.error) {
        console.error(req.error);
        res.status(500).json({
            status: 0,
            error: 'Неизвестная ошибка'
        });
    } else {
        res.status(404).json({
            status: 0,
            error: "Не найдено"
        })
    }
});

export default router;

export type defaultRequest = express.Request & { error: Error }
