import * as express from "express";
import getStations from "../functions/getStations";
import jsonResponseDefault from "../middlewares/jsonResponse";
import addStation from "../functions/addStation";
import editStation from "../functions/editStation";
import getStation from "../functions/getStation";
import deleteStation from "../functions/deleteStation";
import getCountry from "../functions/getCountry";
import getCountries from "../functions/getCountries";

const router = express.Router();

router.get('/stations', jsonResponseDefault(getStations));
router.get('/stations/:id', jsonResponseDefault(getStation));

router.get('/countries', jsonResponseDefault(getCountries));
router.get('/countries/:id', jsonResponseDefault(getCountry));

router.post('/stations', jsonResponseDefault(addStation));
router.patch('/stations/:id', jsonResponseDefault(editStation));
router.delete('/stations/:id', jsonResponseDefault(deleteStation));

export const indexRouter = router;
