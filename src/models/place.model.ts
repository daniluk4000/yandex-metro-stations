import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    Model,
    PrimaryKey,
    Table
} from "sequelize-typescript";
import Station from "./station.model";

@Table({
    modelName: 'place'
})
export default class Place extends Model<Place> {
    @AutoIncrement
    @PrimaryKey
    @Column(DataType.INTEGER)
    id: number;

    @AllowNull(false)
    @ForeignKey(() => Station)
    @Column(DataType.INTEGER)
    stationId: number;

    @BelongsTo(() => Station, {onDelete: 'cascade'})
    station: Station;

    @AllowNull(false)
    @Column(DataType.TEXT)
    placeName: string;

    @Column(DataType.FLOAT)
    lat: number;

    @Column(DataType.FLOAT)
    lon: number
}
