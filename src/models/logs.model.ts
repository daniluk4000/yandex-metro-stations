import {AllowNull, AutoIncrement, Column, DataType, Default, Model, PrimaryKey, Table} from "sequelize-typescript";
import {requestType, sourceType} from "../types";

@Table({
    modelName: 'log',
})
export default class Log extends Model<Log> {
    @AutoIncrement
    @PrimaryKey
    @Column(DataType.INTEGER)
    id: number

    @Default({})
    @Column(DataType.JSON)
    request: { [key: string]: any } | Array<any>

    @Default({})
    @Column(DataType.JSON)
    response: { [key: string]: any } | Array<any>

    @AllowNull(false)
    @Column(DataType.TEXT)
    type: requestType

    @Default(200)
    @Column(DataType.INTEGER)
    status: number

    @Default('public')
    @Column(DataType.TEXT)
    source: sourceType
}
