import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Unique
} from "sequelize-typescript";
import Country from "./country.model";
import Station from "./station.model";

@Table({
    modelName: 'city'
})
export default class City extends Model<City> {
    @AutoIncrement
    @PrimaryKey
    @Column(DataType.INTEGER)
    id: number;

    @AllowNull(false)
    @ForeignKey(() => Country)
    @Column(DataType.INTEGER)
    countryId: number;

    @BelongsTo(() => Country, {onDelete: 'cascade'})
    country: Country;

    @Unique
    @AllowNull(false)
    @Column(DataType.INTEGER)
    cityId: number;

    @AllowNull(false)
    @Column(DataType.TEXT)
    name: string;

    @HasMany(() => Station, {onDelete: 'cascade'})
    stations: Station[]
}
