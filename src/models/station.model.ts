import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    Column,
    DataType,
    Default,
    ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table
} from "sequelize-typescript";
import Place from "./place.model";
import City from "./cities.model";

@Table({
    modelName: 'MetroStation',
    tableName: 'metro_stations'
})
export default class Station extends Model<Station> {
    @AutoIncrement
    @PrimaryKey
    @Column(DataType.INTEGER)
    id: number;

    @AllowNull(false)
    @Column(DataType.TEXT)
    name: string;

    @Column(DataType.FLOAT)
    lat: number;

    @Column(DataType.FLOAT)
    lon: number;

    @AllowNull(false)
    @Default('#000')
    @Column(DataType.TEXT)
    lineColor: string;

    @Default(false)
    @Column(DataType.BOOLEAN)
    exists: boolean;

    @HasMany(() => Place, {onDelete: 'cascade'})
    places: Place[];

    @AllowNull(false)
    @ForeignKey(() => City)
    @Column(DataType.INTEGER)
    cityId: number;

    @BelongsTo(() => City, {onDelete: 'cascade'})
    city: City
}
