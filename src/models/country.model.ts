import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Unique
} from "sequelize-typescript";
import City from "./cities.model";

@Table({
    modelName: 'country'
})
export default class Country extends Model<Country> {
    @AutoIncrement
    @PrimaryKey
    @Column(DataType.INTEGER)
    id: number;

    @Unique
    @AllowNull(false)
    @Column(DataType.INTEGER)
    countryId: number;

    @AllowNull(false)
    @Column(DataType.TEXT)
    name: string;

    @HasMany(() => City, {onDelete: 'cascade'})
    cities: City[]
}
