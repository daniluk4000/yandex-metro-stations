import {Sequelize} from 'sequelize-typescript';

const sequelize = new Sequelize({
    host: '142.93.109.209',
    database: 'postgres',
    username: 'postgres',
    password: '5wwvkt',
    dialect: 'postgres',
    logging: false,
    models: [__dirname + '/*.model.ts', __dirname + '/*.model.js']
});

sequelize.authenticate().then(() => {
    console.info('DB connection has been established')
}).catch((e) => {
    console.error(e);
    process.exit(0);
});

export default sequelize;
